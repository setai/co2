const initialState = {
  geoData: [],
  climateData: []
}

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'INIT_DATA':
    return action.data
  default:
    return state
  }
}

export const dataInit = data => {
  return {
    type: 'INIT_DATA',
    data
  }
}

export default dataReducer
