const initialState = {
  dataType: 'emissions',
  year: 1990,
  country: 'Finland'
}

const stateReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'SET_DATATYPE':
    return { ...state, dataType: action.dataType }
  case 'SET_YEAR':
    return { ...state, year: parseInt(action.year) }
  case 'SET_COUNTRY':
    return { ...state, country: action.country }
  default:
    return state
  }
}

export default stateReducer
