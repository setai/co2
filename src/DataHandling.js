import * as d3 from 'd3'
import * as topojson from 'topojson'
// import * as all_data from './all_data.csv'

export const loadAllData = (callback = () => { }) => {
  Promise.all([
    d3.json('https://unpkg.com/world-atlas@1.1.4/world/50m.json'),
    d3.csv('./all_data.csv', cleanEmissions)
    // d3.csv(all_data, cleanEmissions)
  ]).then(([mapData, data]) => {
    callback({
      geoData: topojson.feature(mapData, mapData.objects.countries).features,
      climateData: data
    })
  })
}

const cleanEmissions = row => ({
  continent: row.Continent,
  country: row.Country,
  countryCode: row['Country Code'],
  emissions: +row['Emissions'],
  emissionsPerCapita: +row['Emissions Per Capita'],
  region: row.Region,
  year: +row.Year
})
