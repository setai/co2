import 'core-js'
import 'whatwg-fetch'
import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import App from './App'
import dataReducer from './reducers/dataReducer'
import stateReducer from './reducers/stateReducer'

const reducer = combineReducers({
  data: dataReducer,
  state: stateReducer
})

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
