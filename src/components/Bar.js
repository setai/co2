import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as d3 from 'd3'

class Bar extends Component {
  constructor() {
    super()
    this.gRef = React.createRef()
  }

  componentDidMount() {
    this.createBar()
    this.d3render()
  }
  componentDidUpdate() {
    this.d3render()
  }

  createBar() {
    const { width, height } = this.props
    const bar = d3.select(this.gRef.current)
    bar.attr('width', width).attr('height', height)

    bar.append('g').classed('x-axis', true)

    bar.append('g').classed('y-axis', true)

    bar
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('x', -height / 2)
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .style('font-size', '1.3em')
      .classed('y-axis-label', true)

    bar
      .append('text')
      .attr('x', width / 2)
      .attr('y', '0em')
      .attr('font-size', '3em')
      .style('text-anchor', 'middle')
      .classed('bar-title', true)
  }

  drawBar() {
    const { data, dataType, country } = this.props
    const bar = d3.select(this.gRef.current)
    const padding = {
      top: 30,
      right: 30,
      bottom: 30,
      left: 110
    }
    const barPadding = 0
    const width = +bar.attr('width')
    const height = +bar.attr('height')
    const countryData = data
      .filter(d => d.country === country)
      .sort((a, b) => a.year - b.year)
    const xScale = d3
      .scaleLinear()
      .domain(d3.extent(data, d => d.year))
      .range([padding.left, width - padding.right])
    const yScale = d3
      .scaleLinear()
      .domain([0, d3.max(countryData, d => d[dataType])])
      .range([height - padding.bottom, padding.top])
    const barWidth = xScale(xScale.domain()[0] + 1) - xScale.range()[0]
    const xAxis = d3.axisBottom(xScale).tickFormat(d3.format('.0f'))

    d3.select('.x-axis')
      .attr('transform', 'translate(0, ' + (height - padding.bottom) + ')')
      .call(xAxis)

    const yAxis = d3.axisLeft(yScale)
    d3.select('.y-axis')
      .attr('transform', 'translate(' + (padding.left - barWidth / 2) + ',0)')
      .transition()
      .duration(1000)
      .call(yAxis)
    const axisLabel =
      dataType === 'emissions'
        ? 'CO₂ emissions, thousand metric tons'
        : 'CO₂ emissions, metric tons per capita'
    const barTitle = country
      ? 'CO₂ Emissions, ' + country
      : 'Click on a country to see annual trends.'

    d3.select('.y-axis-label').text(axisLabel)
    d3.select('.bar-title').text(barTitle)

    const t = d3
      .transition()
      .duration(1000)
      .ease(d3.easeBounceOut)

    const update = bar.selectAll('.bar').data(countryData)

    update
      .exit()
      .transition(t)
      .delay((d, i, nodes) => (nodes.length - i - 1) * 100)
      .attr('y', height - padding.bottom)
      .attr('height', 0)
      .remove()

    update
      .enter()
      .append('rect')
      .classed('bar', true)
      .attr('y', height - padding.bottom)
      .attr('height', 0)
      .merge(update)
      .attr('x', d => (xScale(d.year) + xScale(d.year - 1)) / 2)
      .attr('width', barWidth - barPadding)
      .transition(t)
      .delay((d, i) => i * 50)
      .attr('y', d => yScale(d[dataType]))
      .attr('height', d => height - padding.bottom - yScale(d[dataType]))
      .attr('fill', 'slateGray')
  }

  d3render() {
    this.drawBar()
  }
  render() {
    const { translate: t } = this.props
    return <g transform={`translate(${t[0]}, ${t[1]})`} ref={this.gRef} />
  }
}

const mapStateToProps = state => {
  return {
    data: state.data.climateData,
    dataType: state.state.dataType,
    country: state.state.country
  }
}

const ConnectedBar = connect(
  mapStateToProps,
  {}
)(Bar)
export default ConnectedBar
