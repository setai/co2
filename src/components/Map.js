import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as d3 from 'd3'
import { setCountry } from '../actions/stateActions'

class Map extends Component {
  constructor() {
    super()
    this.gRef = React.createRef()
  }

  componentDidMount() {
    // this.createMap()
    this.d3render()
  }
  componentDidUpdate() {
    this.d3render()
  }

  // createMap() {
  //   d3.select(this.gRef.current)
  //     .append('text')
  //     .attr('x', '50%')
  //     .attr('y', '-30%')
  //     .attr('font-size', '3em')
  //     .style('text-anchor', 'middle')
  //     .classed('map-title', true)
  // }

  setCountryName(countryName) {
    this.props.setCountry(countryName)
  }

  onClick(callback) {
    return function() {
      const country = d3.select(this)
      const countryName = country.data()[0].properties.country
      callback(countryName)
    }
  }

  updateTooltip() {
    const tgt = d3.select(d3.event.target)
    const isCountry = tgt.classed('country')
    let data
    if (isCountry) data = tgt.data()[0].properties

    if (data) {
      const tooltip = d3.select('.tooltip')

      tooltip
        .transition()
        .duration(200)
        .style('opacity', 0.5)
      tooltip
        .style('left', d3.event.pageX - tooltip.node().offsetWidth / 2 + 'px')
        .style('top', d3.event.pageY - tooltip.node().offsetHeight - 10 + 'px')
      tooltip.html(`
              ${data.country}
            `)
      tooltip
        .transition()
        .duration(300)
        .delay(3500)
        .style('opacity', 0)
    }
  }

  getPercentage(d) {
    const angle = d.endAngle - d.startAngle
    const fraction = (100 * angle) / (Math.PI * 2)
    return fraction.toFixed(2) + '%'
  }

  drawMap() {
    const { geoData, climateData, year, dataType } = this.props

    const map = d3.select(this.gRef.current)

    const projection = d3.geoMercator()

    const path = d3.geoPath().projection(projection)

    d3.select('#year-val').text(year)

    geoData.forEach(d => {
      var countries = climateData.filter(row => row.countryCode === d.id)
      var name = ''
      if (countries.length > 0) name = countries[0].country
      d.properties = countries.find(c => c.year === year) || { country: name }
    })

    const colors = ['#f1c40f', '#e67e22', '#e74c3c', '#c0392b']

    const domains = {
      emissions: [0, 2.5e5, 1e6, 5e6],
      emissionsPerCapita: [0, 0.5, 2, 10]
    }

    const mapColorScale = d3
      .scaleLinear()
      .domain(domains[dataType])
      .range(colors)

    const update = map.selectAll('.country').data(geoData)

    update
      .enter()
      .append('path')
      .classed('country', true)
      .attr('d', path)
      .on('click', this.onClick(this.props.setCountry))
      .on('mousemove touchmove', this.updateTooltip)
      .merge(update)
      .transition()
      .duration(750)
      .attr('fill', d => {
        const val = d.properties[dataType]
        return val ? mapColorScale(val) : '#ccc'
      })

    // d3.select('.map-title').text(
    //   'Carbon dioxide ' + this.graphTitle(dataType) + ', ' + year
    // )
  }

  graphTitle(str) {
    return str.replace(/[A-Z]/g, c => ' ' + c.toLowerCase())
  }

  d3render() {
    this.drawMap()
  }

  render() {
    const { translate: t } = this.props
    return <g transform={`translate(${t[0]}, ${t[1]})`} ref={this.gRef} />
  }
}

const mapStateToProps = state => {
  return {
    geoData: state.data.geoData,
    climateData: state.data.climateData,
    dataType: state.state.dataType,
    year: state.state.year
  }
}

const ConnectedMap = connect(
  mapStateToProps,
  { setCountry }
)(Map)
export default ConnectedMap
