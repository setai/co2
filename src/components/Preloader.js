import React from 'react'

import PreloaderImage from '../preloading.png'

const imageStyle = {
  maxWidth: '100%',
  maxHeight: '80vh',
  margin: 'auto'
}

const Preloader = () => (
  <div className="App container">
    <h2 className="text-center">Loading data ...</h2>
    <img src={PreloaderImage} style={imageStyle} alt="Loading preview" />
  </div>
)

export default Preloader
