import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setDataType, setYear } from '../actions/stateActions'
import { extent } from 'd3'

class Nav extends Component {
  constructor() {
    super()
    this.state = {
      extremeYears: [0, 0]
    }
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      extremeYears: extent(this.props.climateData, d => d.year)
    })
  }

  dataTypeClicked = value => () => {
    this.props.setDataType(value)
  }
  yearClicked = event => {
    this.props.setYear(event.target.value)
  }

  render() {
    return (
      <div id="nav">
        <h2>
          CO
          <sub>2</sub> Emissions Dashboard
        </h2>
        <p>
          Explore CO
          <sub>2</sub> emissions by year. Current year: <span id="year-val" />
          <input
            id="year"
            name="year"
            type="range"
            min={this.state.extremeYears[0]}
            max={this.state.extremeYears[1]}
            step="1"
            onChange={this.yearClicked}
          />
        </p>
        <p>
          Choose between total emissions or emissions per capita.
          <input
            type="radio"
            name="dataType"
            onChange={this.dataTypeClicked('emissions')}
            defaultChecked={true}
          />
          <label>Emissions</label>
          <input
            type="radio"
            name="dataType"
            onChange={this.dataTypeClicked('emissionsPerCapita')}
          />
          <label>Emissions Per Capita</label>
        </p>
        <p>Click on a country to see its trends by year.</p>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    climateData: state.data.climateData
  }
}

const mapDispatchToProps = {
  setDataType,
  setYear
}

const ConnectedNav = connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav)

export default ConnectedNav
