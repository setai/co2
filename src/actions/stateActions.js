export const setDataType = dataType => {
  return {
    type: 'SET_DATATYPE',
    dataType
  }
}

export const setYear = year => {
  return {
    type: 'SET_YEAR',
    year
  }
}

export const setCountry = country => {
  return {
    type: 'SET_COUNTRY',
    country
  }
}