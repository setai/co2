export const dataInit = data => {
  return {
    type: 'INIT_DATA',
    data
  }
}
