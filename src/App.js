import React, { Component } from 'react'
import { connect } from 'react-redux'
import Preloader from './components/Preloader'
import { loadAllData } from './DataHandling'
import { dataInit } from './actions/dataActions'
import Nav from './components/Nav'
import Map from './components/Map'
import Bar from './components/Bar'

class App extends Component {
  componentDidMount() {
    loadAllData(data => {
      this.props.dataInit(data)
    })
  }

  render() {
    const { geoData } = this.props

    if (geoData.length < 1) {
      return <Preloader />
    }

    return (
      <div>
        <Nav />
        <div className="tooltip" />
        <svg
          style={{ margin: 'auto', width: '100%' }}
          height={'100vh'}
          viewBox="0 0 1000 750"
        >
          <Map translate={[0, 200]} />
          <svg style={{ opacity: '0.7' }} viewBox="0 0 1900 750">
            <Bar width={750} height={500} translate={[1100, -280]} />
            {/* <Bar width={750} height={500} translate={[50, 200]} /> */}
          </svg>
        </svg>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    geoData: state.data.geoData
  }
}

export default connect(
  mapStateToProps,
  { dataInit }
)(App)
